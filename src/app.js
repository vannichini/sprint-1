const express = require('express');
const server = express();
const { 
    nuevoUsuario, 
    usuariosCreados, 
    inicioSesionUsuario, 
    } = require('../src/database/usuarios');
const { 
    eliminarProducto, 
    productosExistentes, 
    nuevoProducto, 
    modificarProducto 
    } = require('../src/database/productos');
const { 
    esAdmin, 
    emailDuplicado, 
    usuarioExistente
    } = require('../src/middelware/middelware');
const { 
    nuevoMedioDePago, 
    modificarMedioDePago, 
    todosLosMediosDePago, 
    eliminarMedioDePago
    } = require('../src/database/mediosdepago');

const { 
    nuevoPedido, 
    historialPedidos, 
    todosLosPedidos, 
    modificarEstadoPedido 
} = require ('../src/database/pedidos');

const { informacionSwagger } = require('./middelware/swagger');

server.use(express.json());

informacionSwagger(server);

// ----> Path: Usuarios. Todas las acciones que se pueden llevar adelante con los mismos <----
server.post('/usuarios', emailDuplicado, nuevoUsuario);
server.post('/usuarioslogin', inicioSesionUsuario);
server.use(usuarioExistente);
server.get('/usuarios', esAdmin, usuariosCreados);

// ----> Path: Pedidos. Todas las acciones que se pueden llevar adelante con los mismos <----
server.get('/pedidos', esAdmin, todosLosPedidos);
server.post('/pedidos', nuevoPedido);
server.get('/pedidos/historial', historialPedidos);
server.put('/pedidos/:id', esAdmin, modificarEstadoPedido);

// ----> Path: Productos. Todas las acciones que se pueden llevar adelante con los mismos <----
server.post('/productos', esAdmin, nuevoProducto);
server.put('/productos/:idProd', esAdmin, modificarProducto);
server.get('/productos', productosExistentes);
server.delete('/productos/:idPro', esAdmin, eliminarProducto);

// ----> Path: Medios de pago. Todas las acciones que se pueden llevar adelante con los mismos. Solo puede ser usado por un usuario administrador <----
server.use('/mediosdepago', esAdmin);
server.post('/mediosdepago', nuevoMedioDePago);
server.get('/mediosdepago', todosLosMediosDePago);
server.put('/mediosdepago/:id', modificarMedioDePago);
server.delete('/mediosdepago/:id', eliminarMedioDePago);

server.listen(9000, () => {console.log('Servidor Iniciado')});