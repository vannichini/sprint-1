const yaml = require('js-yaml');
const fs = require('fs');
const swaggerUi = require('swagger-ui-express');

function informacionSwagger(server) {
    try {
        const documento = yaml.load(fs.readFileSync('./src/spec.yml','utf-8'));
        server.use('/api-doc', swaggerUi.serve, swaggerUi.setup(documento));
    } catch (e) {
        console.log(e);
    }
}

module.exports = {
    informacionSwagger
}
