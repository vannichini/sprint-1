const { usuarios } = require("../database/usuarios");

function esAdmin(req, res, next) {
    let idAdmin = req.headers.id;
    if (usuarios[idAdmin].admin) {
        next();
        return;
    }
    res.send('usted no tiene autorización para continuar');
}

function emailDuplicado(req, res, next) {
    let emailDado = req.body.mailAdress;
    let usernameDado = req.body.username;
    for (const usuario of usuarios) {
        if (usuario.mailAdress === emailDado || usuario.username === usernameDado) {
            res.send('email o username duplicado');
            return;
        }
    }
    next();
}

function usuarioExistente (req, res, next){
    const idUsuario = Number(req.headers.id);
    const usuariosTotales = usuarios.length -1;
        if (usuariosTotales >= idUsuario) {
            next();
            return;
        }
    res.send('no es un usuario existente');
}


module.exports = {
    esAdmin,
    emailDuplicado, 
    usuarioExistente
}