let productos =
    [{
        idProducto: 1,
        nombre: 'Baguel de salmón',
        precio: 425
    },
    {
        idProducto: 2,
        nombre: 'Hamburguesa clásica',
        precio: 350
    },
    {
        idProducto: 3,
        nombre: 'Sandwich veggie',
        precio: 310
    },
    {
        idProducto: 4,
        nombre: 'Ensalada veggie',
        precio: 340
    },
    {
        idProducto: 5,
        nombre: 'Focaccia',
        precio: 300
    },
    {
        idProducto: 6,
        nombre: 'Sandwich Focaccia',
        precio: 440
    }
    ]

class Productos {
    constructor(idProducto, nombre, precio) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.precio = precio;
    }
}

function crearProducto(idProducto, nombre, precio) {
    let productoNuevo = new Productos(idProducto, nombre, precio);
    return productoNuevo;
}

function productosExistentes(req, res) {
    let todosProductos = [];
    for (const producto of productos) {
        todosProductos.push(producto.nombre + ' ' + producto.precio);
    }
    res.send(todosProductos);
}

function eliminarProducto(req, res) {
    const idDado = req.params.idPro;
    for (const producto of productos) {
        const posicionProducto = productos.indexOf(producto);
        if (idDado == producto.idProducto) {
            productos.splice(posicionProducto, 1);
            res.send('producto eliminado');
            return;
        }
    }
    res.send('producto no encontrado');
}

function nuevoProducto(req, res) {
    let nuevoId = productos[productos.length - 1].idProducto + 1;
    let nuevoNombre = req.body.nombre;
    let nuevoPrecio = Number(req.body.precio);
    productos.push(crearProducto(nuevoId, nuevoNombre, nuevoPrecio));
    res.status(202).send('nuevo producto creado');
}

function modificarProducto(req, res) {
    let idDado = req.params.idProd
    for (const producto of productos) {
        if (idDado == producto.idProducto) {
            producto.nombre = req.body.nombre;
            producto.precio = Number(req.body.precio);
            res.send('producto modificado');
            return;
        }
    }
    res.send('producto no existente');
}

module.exports = { 
    productos, 
    eliminarProducto, 
    productosExistentes, 
    nuevoProducto, 
    modificarProducto 
    }