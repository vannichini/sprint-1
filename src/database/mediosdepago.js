let mediosDePago = [
    {
        idMedioDePago : 1,
        formaDePago : 'efectivo',
    },
    {
        idMedioDePago : 2,
        formaDePago : 'tarjeta de debito',
    },
    {
        idMedioDePago: 3,
        formaDePago: 'tarjeta de credito',
    },
    {
        idMedioDePago: 4,
        formaDePago: 'mercado pago',
    }
]

class Mediodepago {
    constructor(idMedioDePago, formaDePago) {
        this.id = idMedioDePago;
        this.formaDePago = formaDePago;
    }
}
function crearMedioDePago(idMedioDePago, formaDePago) {
    let newFormaDePago = new Mediodepago(idMedioDePago, formaDePago);
    return (newFormaDePago);
}

function todosLosMediosDePago(req, res) {
    let losMediosDePago = [];
    for (const medio of mediosDePago) {
        losMediosDePago.push(medio.formaDePago);
    }
    res.send(losMediosDePago);
}

function nuevoMedioDePago(req, res) {
    let idNuevo = mediosDePago[mediosDePago.length - 1].idMedioDePago + 1;
    let nuevaFormaDePago = req.body.formaDePago
    mediosDePago.push(crearMedioDePago(idNuevo, nuevaFormaDePago));
    res.status(202).send('nuevo medio de pago creado');
}

function modificarMedioDePago(req, res) {
    let idIndicado = req.params.id
    for (const medio of mediosDePago) {
        if (idIndicado == medio.idMedioDePago) {
            medio.formaDePago = req.body.formaDePago
            res.send('forma de pago editada')
            return;
        }
    }
    res.send('medio de pago no encontrado');
}

function eliminarMedioDePago(req, res) {
    let idIndicado = req.params.idMedio
    for (const medio of mediosDePago) {
        const posicionMedioDePago = mediosDePago.indexOf(medio);
        if (idIndicado = medio.idMedioDePago) {
            mediosDePago.splice(posicionMedioDePago, 1);
            res.send('medio de pago eliminado');
            return
        }
    }
    res.send('medio de pago no encontrado');
}

module.exports = {
    mediosDePago,
    nuevoMedioDePago,
    modificarMedioDePago,
    todosLosMediosDePago,
    eliminarMedioDePago
}