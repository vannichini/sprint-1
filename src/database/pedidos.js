const { usuarios } = require ('../database/usuarios');
const { productos } = require ('../database/productos');
const { mediosDePago } = require ('../database/mediosdepago');

let pedidos = [
    {
        id: 1,
        usuario: 0,
        direccionDeEnvio: "perito moreno 4880",
        formaDePago: 1,
        precioTotal: 850,
        productos: [{
            idProducto: 1,
            cantidad: 2,
        }],
        estado: "pedido entregado"
    }
]

let estadoPedidos = [
    {
        idEstado: 1,
        estado: "pedido pendiente"
    },
    {
        idEstado: 2,
        estado: "pedido confirmado"
    },
    {
        idEstado: 3,
        estado: "pedido en preparacion"
    },
    {
        idEstado: 4,
        estado: "pedido enviado"
    },
    {
        idEstado: 5,
        estado: "pedido entregado"
    }
]

class Pedidos {
    constructor (productos, usuario, precioTotal, formaDePago, direccionDeEnvio, id, estado){
        this.productos = productos;
        this.usuario = usuario;
        this.precioTotal = precioTotal;
        this.formaDePago = formaDePago;
        this.direccionDeEnvio = direccionDeEnvio;
        this.id = id;
        this.estado = estado;
    }
}

function crearPedido (productos, usuario, precioTotal, formaDePago, direccionDeEnvio, id, estado){
    const pedidoNuevo = new Pedidos (productos, usuario, precioTotal, formaDePago, direccionDeEnvio, id, estado)
    pedidos.push(pedidoNuevo);
}

function chequearDireccion (dire, idUsuario){
    if (dire === "") {
        return usuarios[idUsuario].adress;
    }
    else {
        return dire;
    }
}

function pagoTotal (productosUsuario){
    let precioFinal = 0;
    for (const producto of productosUsuario) {
        for (const productoOriginal of productos) {
            if (producto.idProducto === productoOriginal.idProducto) {
                precioFinal = precioFinal + (productoOriginal.precio * producto.cantidad);
            }
        }
    }
    return precioFinal;
}

function nuevoPedido (req,res){
    const usuarioPedido = Number(req.headers.id);
    const idPedido = pedidos[pedidos.length - 1].id + 1; 
    const direccionEnvio = chequearDireccion(req.body.dirNueva, usuarioPedido);
    const formaPago = req.body.formaDePago;
    const productoUsuario = req.body.productosUsuario;
    const precioTotal = pagoTotal(productoUsuario);
    const estado = "pedido confirmado";
    crearPedido(productoUsuario,usuarioPedido,precioTotal,formaPago,direccionEnvio,idPedido, estado);
    res.send('Nuevo pedido confirmado');
}

function historialPedidos (req, res){
    const idDelUsuario = Number(req.headers.id);
    const historicoPedidos = [];
    for (const pedido of pedidos) {
        if (idDelUsuario === pedido.usuario) {
            historicoPedidos.push(pedido);
        }
    }
    res.send(historicoPedidos);
}

function todosLosPedidos(req, res){
    res.send(pedidos);
}

function modificarEstadoPedido(req, res){
    const idPedido = Number(req.params.id);
    const idEstado = Number(req.headers.estado);
    for (const pedido of pedidos) {
        if (idPedido === pedido.id) {
            for (const estado of estadoPedidos){
                if (estado.idEstado === idEstado) {
                    pedido.estado = estado.estado;
                    res.send(pedido.estado);
                    return;
                }
            } 
            res.send('estado no existente');
            return;
        }
    }
    res.send('pedido no encontrado');
}

module.exports = {
    nuevoPedido,
    historialPedidos,
    todosLosPedidos,
    modificarEstadoPedido
}