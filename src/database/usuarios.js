let usuarios =
    [
        {
            username: 'vannichini',
            fullName: 'Valentina Annichini',
            mailAdress: 'annichiniv@gmail.com',
            phone: 1165495027,
            adress: 'Perito Moreno 4880',
            password: 'rita123',
            admin: true
        },
        {
            username: 'emiluis',
            fullName: 'Emiliano Damian Luis',
            mailAdress: 'emiluis@gmail.com',
            phone: 2901605738,
            adress: 'Quinquela Martin 1919',
            password: '1990emi',
            admin: false          
        }
    ];

class Usuario {
    constructor(username, fullName, mailAdress, phone, adress, password, admin) {
        this.username = username;
        this.fullName = fullName;
        this.mailAdress = mailAdress;
        this.phone = phone;
        this.adress = adress;
        this.password = password;
        this.admin = admin;
    }
}

function nuevoUsuario(req, res) {
    const nuevousername = req.body.username;
    const nuevofullName = req.body.fullName;
    const nuevomailAdress = req.body.mailAdress;
    const nuevoPhone = req.body.phone;
    const nuevoAdress = req.body.adress;
    const nuevoPassword = req.body.password;
    const admin = false;
    if (nuevousername && nuevofullName && nuevomailAdress && nuevoPhone && nuevoAdress && nuevoPassword) {
        const newUsuario = new Usuario(nuevousername, nuevofullName, nuevomailAdress, nuevoPhone, nuevoAdress, nuevoPassword, admin);
        usuarios.push(newUsuario);
        res.status(202).send(`Nuevo usuario creado ${JSON.stringify(newUsuario)}`);
        return;
    } else {
        res.send('faltan completar datos');
        return;
    }
}

function usuariosCreados(req, res) {
    let usuariosExistentes = [];
    for (const usuario of usuarios) {
        usuariosExistentes.push(usuario.username + ' ' + usuario.mailAdress);
    }
    res.send(usuariosExistentes);
}

function inicioSesionUsuario(req, res) {
    let usuarioDado = req.headers.username;
    let contra = req.headers.password;
    for (const usuario of usuarios) {
        if (usuarioDado == usuario.username && contra == usuario.password) {
            let posicionUsuario = usuarios.indexOf(usuario)
            res.send(`Id del usuario ${posicionUsuario}`);
            return;
        }
    }
    res.send('usuario no encontrado');
}

module.exports = { 
    usuarios, 
    nuevoUsuario, 
    usuariosCreados, 
    inicioSesionUsuario }