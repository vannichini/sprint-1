# API Delilah Restó
El Sprint 1 indicaba la creación de una API para el manejo de pedidos, productos y usuarios del restaurante Delilah Restó.
## Instalación
Se puede correr la API utilizando NodeJS. 

[Node.js](https://nodejs.org/)

Para poder utilizar la API Delilah Restó se deben instalar las librerias detalladas debajo:

- [YAML](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html)
- [Express](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/Introduction)
- [Swagger](https://swagger.io/docs/specification/about/)

Las librerias mencionadas anteriormente deben instalarse desde el repositorio. Los comandos para instalar cada libreria son los detallados debajo:
```
npm i js-yaml

npm i express

npm install swagger-ui-express
```
Una vez completa la instalación de las librerias, para poder dar inicio el servidor se debe utilizar el siguiente comando:
```
npm start
```
Una vez iniciado, el servidor escucha el puerto 9000. Es posible modificarlo en el archivo app.js, en la linea 60 donde se encuentra de la siguiente forma:

```javascript
server.listen(9000, () => {console.log('Servidor Iniciado')});
```

## Testeo

De forma que se pueda hacer testeo de la API se unio la documentación con swagger, en cuanto el servidor se encuentre funcionando, se va a poder acceder desde el siguiente link:

- [API - Swagger](http://localhost:9000/api-doc/)

En caso de querer realizar un testeo desde la terminal, se puede utilizar el siguiente comando, para el cual se debe instalar la libreria 
[Nodemon](https://www.npmjs.com/package/nodemon)
```
npm run dev
```


### Usuarios
Para poder testear todas las aplicaciones posibles, cuentan con un usuario administrador, que puede realizar todas las funciones. 

También se cuenta con un usuario cliente, que sirve de tester para comprobar todas las funciones posibles.

#### _Usuario Administrador_

- Username: vannichini
- Password: rita123

#### _Usuario Cliente_
- Username: emiluis
- Password: 1990emi


### Id de Usuario

Para realizar login y para luego poder confirmar su usuario, se utiliza un ID por usuario, el mismo es la posición que tiene el usuario dentro del array de Usuarios. 

Es devuelto al momento de ejecutar la función de Inicio de Sesión. Puede ser requerido por los ciertos endpoint como parametro de cabezera (headers).

### Emails duplicados
La validación de emails duplicados o nombres de usuario, se realiza al momento de crear un nuevo usuario, por medio de un middelware.

### Dirección alternativa de envio

Si los usuarios quisieran utilizar una dirección de envio distinta a la registrada al momento de crear la cuenta, tienen la posibilidad de hacerlo al momento de hacer un nuevo pedido.

En caso de no registrar una nueva dirección, el sistema toma por defecto la dirección ya cargada.
### Autora

Valentina Annichini

_Licencia_
MIT